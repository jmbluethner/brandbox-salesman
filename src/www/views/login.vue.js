/**
 * @author Jan-Morris Blüthner <bluethner@heliophobix.com>
 */

const Vue = require('vue');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync('./db/db.json');
const db = low(adapter);

import {consoleDebug} from '../js/components/logging.js';

consoleDebug('Test');

/**
function createTestDataset(name) {
    const connection = {
        id: 1,
        host: 'ssh.local.host',
        username: 'root',
        post: 22,
        pass: 1970,
        name: name
    }
    db.get('connections').push(connection).write();
}
 */

var bus = {};
bus.eventBus = new Vue();

var app = new Vue({
    el: '#app',
    data: {},

    methods: {},
    created: function () {

    },
    components: {
        'login': {
            data: function () {
                return {

                }
            },
            methods:  {

            },
            created: function() {

            },
            template:
                ''
        }
    }
});