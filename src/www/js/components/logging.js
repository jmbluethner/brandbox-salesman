/**
 * Import
 */

import {Settings} from '../../../config/config.js';

/**
 * @description This function logs to the console if debug mode is enabled
 * @param {object} obj Whatever you want to log
 */
export function consoleDebug(obj) {
    if(Settings.debugMode) {
        console.log(obj);
    }
}